@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <!--  顯示順序反過來排 -->
            @foreach(App\Models\Post::orderBy('created_at','DESC')->get() as $post)
                <!-- 顯示資料庫資料 -->
                <div class="card">
                    <div class="card-header">
                        #{{ $post->id }}
                        {{ $post -> title }} @ {{ $post->created_at }}
                        <!-- 編輯文件 -->
                        @auth
                        <!-- auth可以保證未登入的情況下看不到貼文 -->
                        <a href="{{ route('posts.edit', [$post->id]) }}">(Edit)</a>
                        @endauth
                    </div>
                    <div class="card-body">
                        <!-- 若使用者輸入的是js代碼加驚嘆號可以顯示js的功能 -->
                        <!-- {!! nl2br($post -> content) !!} -->
                        {{($post -> content)}}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
