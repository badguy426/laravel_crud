<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return "hihi";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ //驗證說有沒有少打title或content
            'title' => 'required',
            'content' => 'required',
        ]);
        $post = new Post;
        $post->title = request('title');
        $post->content = request('content');
        $post->user_id = \Auth::id();
        $post->save();

        return redirect()->to('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        // dd($post);//把資料拿出來看
        return view('post.edit',compact('post'));//把post資料丟進去edit裡面
        //也可以用with的方式然後再php中用session去接跟update一樣
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([ //驗證說有沒有少打title或content
            'title' => 'required',
            'content' => 'required',
        ]);

        $post = new Post;
        $post->title = request('title');
        $post->content = request('content');
        $post->user_id = \Auth::id();
        $post->save();
        return redirect()->route('posts.edit',[$post->id])->with('success',true);
        // 更新完後不想跳轉頁面，所以回到編輯的頁面，並顯示一個成功訊息
        // 用redirect把with回來的話可以透過session去接
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->to('/');
    }
}
